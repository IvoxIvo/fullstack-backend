export interface CategoryData {
  getCategoryBy(
    search: string,
    colum: string,
    limit: number,
    offset: number
  ): any;
  getSearchCategoryCount(search: string, column: string): any;
  getAllCategories(limit: number, offset: number): any;
  allCategoriesCount(): any;
  categoryBy(column: string, getBy: string | number): any;
  allCategories(): any;
  allCategoryImages(): any;
  createCategory(
    name: string,
    image: number,
    userId: number,
    description: string | undefined
  ): any;
  quizzesOfCategory(id: string, userId: number): any;
}
