export interface ScoresData {
  getAllScores(limit: number, offset: number): any;
  getAllScoresCount(): any;
  searchScoresBy(search: string, limit: number, offset: number): any;
  searchScoresCount(search: string): any;
  topScores(): any;
  findQuiz(id: number): any;
  quizScores(id: number, limit: number, offset: number): any;
  quizScoresCount(id: number): any;
}
