export interface UsersData {
  createUser(
    username: string,
    password: string,
    firstname: string,
    lastname: string
  ): any;
  getUserBy(column: string, searchBy: string): any;
  getAllUsers(limit: number, offset: number): any;
  allUsersCount(): any;
  getUsersBy(
    searchBy: string,
    column: string,
    limit: number,
    offset: number
  ): any;
  getSearchCount(searchBy: string, column: string): any;
}
