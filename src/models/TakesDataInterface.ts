export interface TakesData {
  findUserTake(id: number, userId: number): any;
  newTake(id: number, userId: number, startingTime: Date): any;
  getActiveQuiz(userId: number): any;
  submitAnswers(id: number, answers: string[]): any;
  submitQuiz(id: number, userId: number, totalPoints: number): any;
  getStudentScore(id: number): any;
  getScoreOnTake(id: number, userId: number): any;
  getLastUserScores(id: number): any;
  getCorrectAnswers(id: number): any;
  getUserAnswers(id: number, userId: number, takeId: number): any;
  getUserScores(userId: number, limit: number, offset: number): any;
  getUserScoresCount(userId: number): any;
  searchUserScoresBy(
    userId: number,
    search: string,
    limit: number,
    offset: number
  ): any;
  searchUserScoresCount(userId: number, search: string): any;
}
