import { Answer } from '../models/quizTypes';
import { Question, Quiz, RawQuizData } from '../models/quizTypes';

const quizFormatter = (quiz: RawQuizData[]) => {
  const formattedQuiz: Quiz = {
    id: quiz[0].quizID,
    userId: quiz[0].quizCreatorID,
    category: quiz[0].quizCategory,
    title: quiz[0].quizTitle,
    timelimit: quiz[0].timelimit,
    questions: [],
  };

  const questions = quiz.reduce((acc: Question[], value) => {
    const qExists = acc.some((q) => q.id === value.questionID);
    if (!qExists) {
      const question: Question = {
        id: value.questionID,
        name: value.questionText,
        points: value.questionPoints,
        answers: [],
      };
      const answers = quiz.reduce((acc: Answer[], value) => {
        if (value.questionID === question.id) {
          const answer = {
            id: value.answerID,
            text: value.answerText,
            is_correct: value.is_correct,
          };
          acc.push(answer);
        }
        return acc;
      }, []);
      question.answers = [...answers];
      acc.push(question);
    }
    return acc;
  }, []);

  formattedQuiz.questions = [...questions];
  return formattedQuiz;
};
export default quizFormatter;
