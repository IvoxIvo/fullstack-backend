import { PoolConfig } from 'mariadb';
import dotenv from 'dotenv';
dotenv.config();

export const DATABASE_CONFIG: PoolConfig = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: process.env.DB_PASSWORD,
  database: 'quizz-app',
  multipleStatements: true,
};

export const PORT = 3000;

export const PRIVATE_KEY = process.env.PRIVATE_KEY!;

export const TOKEN_LIFETIME = 600 * 60;

export const teachersOnly: [string] = ['teacher'];

export const registeredUsers: [string, string] = ['student', 'teacher'];
