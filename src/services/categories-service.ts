import serviceErrors from './service-errors';
import { CategoryData } from '../models/CategoryDataInterface';

const getAllCategoriesPaged = (data: CategoryData) => {
  return async (search: undefined | string, column: string, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    if (search) {
      const category = await data.getCategoryBy(search, column, limit, offset);

      const [{ count }] = await data.getSearchCategoryCount(search, column);

      return {
        category: category,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    } else {
      const category = await data.getAllCategories(limit, offset);
      const [{ count }] = await data.allCategoriesCount();

      return {
        category: category,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    }
  };
};

const getCategoryById = (data: CategoryData) => {
  return async (id: string) => {
    const category = await data.categoryBy('id', id);
    if (!category) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        category: null,
      };
    }

    return { error: null, category: category };
  };
};

const getAllCategories = (data: CategoryData) => {
  return async () => {
    const categories = await data.allCategories();
    return categories;
  };
};
const getCategoryImages = (data: CategoryData) => {
  return async () => {
    const images = await data.allCategoryImages();
    return images;
  };
};

const createCategory = (data: CategoryData) => {
  return async (
    categoryData: {
      name: string;
      images_id: string;
      description: string | undefined;
    },
    userId: number
  ) => {
    const { name, description, images_id } = categoryData;

    const existingCategory = await data.categoryBy('name', name);

    if (existingCategory) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        category: null,
      };
    }

    const category = await data.createCategory(
      name,
      +images_id,
      +userId,
      description
    );
    return { error: null, category: category };
  };
};

const getQuizzesOfCategory = (data: CategoryData) => {
  return async (
    id: string,
    user: {
      id?: string;
    }
  ) => {
    const userId = user.id!;

    const quizzes = await data.quizzesOfCategory(id, +userId);

    if (!quizzes[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        quizzes: null,
      };
    }
    return {
      error: null,
      quizzes: quizzes,
    };
  };
};

export default {
  getAllCategoriesPaged,
  getAllCategories,
  getCategoryById,
  getCategoryImages,
  createCategory,
  getQuizzesOfCategory,
};
