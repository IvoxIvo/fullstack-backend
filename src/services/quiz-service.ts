import { Quiz } from '../models/quizTypes';
import quizFormatter from '../utils/quizFormatter';
import serviceErrors from './service-errors';

const getQuiz = (data: any) => {
  return async (quizId: number) => {
    const rawQuizData = await data.getQuizById(quizId);

    if (!rawQuizData[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        quiz: null,
      };
    } else {
      const formattedQuiz = quizFormatter(rawQuizData);
      return {
        error: null,
        quiz: formattedQuiz,
      };
    }
  };
};
const getQuizzesByUser = (data: any) => {
  return async (userId: number) => {
    const quizzesData = await data.getUserQuizzes(userId);

    if (!quizzesData[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        quizzes: [],
      };
    } else {
      return {
        error: null,
        quizzes: quizzesData,
      };
    }
  };
};
const getQuizSubmissions = (data: any) => {
  return async (quizId: number) => {
    const submissions = await data.quizSubmissions(quizId);

    if (!submissions[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        submissions: [],
      };
    } else {
      return {
        error: null,
        submissions: submissions,
      };
    }
  };
};
const getAllQuizzes = (data: any) => {
  return async () => {
    const allQuizzes = await data.getQuizzes();

    if (!allQuizzes[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        quiz: null,
      };
    } else {
      return {
        error: null,
        quiz: allQuizzes,
      };
    }
  };
};

const checkQuiz = (data: any) => {
  return async (name: string) => {
    const quiz = await data.checkQuizByName(name);

    if (quiz) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        quiz: null,
      };
    } else {
      return {
        error: null,
        quiz: quiz,
      };
    }
  };
};

const postQuiz = (data: any) => {
  return async (quiz: Quiz) => {
    const { title } = quiz;

    // check for existing quiz with the same name
    const exists = await data.checkQuizByName(title);

    if (exists) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        quiz: null,
      };
    } else {
      const _ = await data.createQuiz(quiz);
      return {
        error: null,
        quiz: `A new quiz with title '${title}' has been successfully created!`,
      };
    }
  };
};

export default {
  getQuiz,
  getQuizzesByUser,
  getQuizSubmissions,
  getAllQuizzes,
  checkQuiz,
  postQuiz,
};
