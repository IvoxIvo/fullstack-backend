import serviceErrors from './service-errors';
import { ScoresData } from '../models/ScoresDataInterface';
import { compareSync } from 'bcrypt';

const getScores = (data: ScoresData) => {
  return async (search: undefined | string, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    if (search) {
      const result = await data.searchScoresBy(search, limit, offset);
      const countResult = await data.searchScoresCount(search);
      let count = 0;

      if (countResult[0] && countResult[0].count) {
        count = countResult[0].count;
      }

      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    } else {
      const result = await data.getAllScores(limit, offset);
      const countResult = await data.getAllScoresCount();
      let count = 0;

      if (countResult[0] && countResult[0].count) {
        count = countResult[0].count;
      }

      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: page > 1,
      };
    }
  };
};

const getTopScores = (data: ScoresData) => {
  return async () => {
    const result = await data.topScores();
    return result;
  };
};

const getQuizScores = (data: ScoresData) => {
  return async (id: number, page: number) => {
    const limit = 20;
    const offset = (page - 1) * limit;

    const quiz = await data.findQuiz(id);
    if (!quiz) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }

    const count = await data.quizScoresCount(id);

    const result = await data.quizScores(id, limit, offset);
    return {
      result: result,
      count: count,
      currentPage: page,
      hasNext: offset + limit < count,
      hasPrevious: page > 1,
    };
  };
};

export default {
  getScores,
  getTopScores,
  getQuizScores,
};
