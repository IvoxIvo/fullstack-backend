import serviceErrors from './service-errors';
import { TakesData } from '../models/TakesDataInterface';

const takeQuiz = (data: TakesData) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    }
  ) => {
    const alreadyTaken = await data.findUserTake(+id, +user.id!);

    if (alreadyTaken && user.role !== 'teacher') {
      return {
        error: serviceErrors.ALREADY_TAKEN,
        take: null,
      };
    }

    const activeQuiz = await data.getActiveQuiz(+user.id!);
    if (activeQuiz) {
      return {
        error: serviceErrors.ANOTHER_ACTIVE_QUIZ,
        take: null,
      };
    }

    const startingTime = new Date();
    const take = await data.newTake(id, +user.id!, startingTime);

    return { error: null, take: take };
  };
};

const submitQuiz = (data: TakesData) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    },
    answers: string[]
  ) => {
    const userId: number = +user.id!;
    const activeQuiz = await data.getActiveQuiz(userId);

    if (!activeQuiz || +activeQuiz.quiz_id !== +id) {
      return {
        error: serviceErrors.INVALID_OPERATION,
        submission: null,
      };
    }

    const takeId = activeQuiz.id;
    const correctAnswers = new Map();
    const pointsPerAnswers = new Map();
    const answersToCheck = new Map();
    let totalPoints = 0;
    let maxPoints = 0;

    const _ = await data.submitAnswers(takeId, answers);

    const correctAnswersAndPoints = await data.getCorrectAnswers(id);
    if (!correctAnswersAndPoints[0]) {
      return {
        error: serviceErrors.INVALID_OPERATION,
        score: null,
      };
    }

    const userAnswers = await data.getUserAnswers(id, +user.id!, +takeId);

    correctAnswersAndPoints.forEach(
      (e: {
        question_id: number;
        answers_id: number;
        points: number;
        text: string;
      }) => {
        if (correctAnswers.has(e.question_id)) {
          const oldValue = correctAnswers.get(e.question_id);
          correctAnswers.set(e.question_id, [...oldValue, e.answers_id]);
        } else {
          correctAnswers.set(e.question_id, [e.answers_id]);
        }
        if (!pointsPerAnswers.has(e.question_id)) {
          pointsPerAnswers.set(e.question_id, e.points);
        }
      }
    );

    pointsPerAnswers.forEach((e) => (maxPoints += e));

    if (answers.length === 0) {
      const submission = await data.submitQuiz(id, userId, 0);

      return {
        error: null,
        submission: {
          result: 0,
          maxPoints: maxPoints,
          takeId: takeId,
        },
      };
    }

    userAnswers.forEach(
      (e: { question_id: number; answer_from_take: number; text: string }) => {
        if (answersToCheck.has(e.question_id)) {
          const oldValue = answersToCheck.get(e.question_id);
          answersToCheck.set(e.question_id, [...oldValue, e.answer_from_take]);
        } else {
          answersToCheck.set(e.question_id, [e.answer_from_take]);
        }
      }
    );

    correctAnswers.forEach((value, key) => {
      const correctAnswer = [...value];
      if (answersToCheck.has(key)) {
        const userAnswer = [...answersToCheck.get(key)];
        if (
          JSON.stringify(correctAnswer.sort()) ==
          JSON.stringify(userAnswer.sort())
        ) {
          totalPoints += pointsPerAnswers.get(key);
        }
      }
    });

    const submission = await data.submitQuiz(id, userId, totalPoints);

    return {
      error: null,
      submission: { result: totalPoints, maxPoints: maxPoints, takeId: takeId },
    };
  };
};

const studentScore = (data: TakesData) => {
  return async (user: { role?: string; id?: string }) => {
    const userId = user.id!;

    const result = await data.getStudentScore(+userId);

    if (!result[0].avg_result) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }
    return { error: null, result: result };
  };
};

const scoreOnTake = (data: TakesData) => {
  return async (
    id: number,
    user: {
      role?: string;
      id?: string;
    }
  ) => {
    const userId = user.id!;

    const record = await data.findUserTake(+id, +userId);
    if (!record) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }
    const result = await data.getScoreOnTake(+id, +userId);

    return {
      error: null,
      result: result,
    };
  };
};

const lastScores = (data: TakesData) => {
  return async (user: { role?: string; id?: string }) => {
    const userId = user.id;
    const result = await data.getLastUserScores(+userId!);

    if (result && !result[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        result: null,
      };
    }
    return {
      error: null,
      result: result,
    };
  };
};

const allScores = (data: TakesData) => {
  return async (
    user: { role?: string; id?: string },
    search: string | null,
    page: string
  ) => {
    const userId = user.id;
    const limit = 20;
    const offset = (+page - 1) * limit;

    if (search) {
      const result = await data.searchUserScoresBy(
        +userId!,
        search,
        limit,
        offset
      );
      const [{ count }] = await data.searchUserScoresCount(+userId!, search);
      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: +page > 1,
      };
    } else {
      const result = await data.getUserScores(+userId!, limit, offset);
      const [{ count }] = await data.getUserScoresCount(+userId!);

      return {
        result: result,
        count: count,
        currentPage: page,
        hasNext: offset + limit < count,
        hasPrevious: +page > 1,
      };
    }
  };
};

export default {
  takeQuiz,
  submitQuiz,
  studentScore,
  scoreOnTake,
  lastScores,
  allScores,
};
