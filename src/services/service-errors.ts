export default {
  /** Such a record does not exist (when it is expected to exist) */
  RECORD_NOT_FOUND: 1,
  /** The requirements do not allow more than one of that resource */
  DUPLICATE_RECORD: 2,
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: 3,
  /** username/password mismatch */
  INVALID_SIGNIN: 4,
  /** student already took quiz */
  ALREADY_TAKEN: 5,
  /** student has another active quiz */
  ANOTHER_ACTIVE_QUIZ: 6,
  /** invalid operation request */
  INVALID_OPERATION: 7,
};
