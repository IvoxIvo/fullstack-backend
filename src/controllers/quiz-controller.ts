import express, { Request, Response } from 'express';
import serviceErrors from '../services/service-errors';
import quizData from '../data/quiz-data';
import quizService from '../services/quiz-service';
import {
  bodyValidator,
  createQuizSchema,
} from '../validations/validation-exports';
import { Quiz } from '../models/quizTypes';

const quizController = express.Router();

quizController
  // Get quiz by id
  .get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    const { error, quiz } = await quizService.getQuiz(quizData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({
        message: `Quiz with an id ${id} does not exist!`,
      });
    }

    res.status(200).send(quiz);
  })
  // Get quizzes by user
  .get('/user/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const { error, quizzes } = await quizService.getQuizzesByUser(quizData)(
      +id
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({
        message: `User with an id ${id} does not exist!`,
      });
    }

    res.status(200).send(quizzes);
  })
  // Get quiz submissions
  .get('/submissions/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const { error, submissions } = await quizService.getQuizSubmissions(
      quizData
    )(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({
        message: `Quiz with an id ${id} does not exist!`,
      });
    }

    res.status(200).send(submissions);
  })

  // Get all quizzes
  .get('/', async (req: Request, res: Response) => {
    const { error, quiz } = await quizService.getAllQuizzes(quizData)();
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({
        message: `There are no quizzes!`,
      });
    }
    res.status(200).send(quiz);
  })

  // Post (create) new quiz
  .post(
    '/',
    bodyValidator(createQuizSchema),
    async (req: Request, res: Response) => {
      const data: Quiz = req.body;
      const { title } = data;

      const { error, quiz } = await quizService.postQuiz(quizData)(data);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({
          message: `Quiz with name '${title}' already exists!`,
        });
      } else {
        res.status(200).send({ message: quiz });
      }
    }
  );

export default quizController;
