import { connect } from 'http2';
import { Quiz } from '../models/quizTypes.js';
import pool from './pool.js';

const getQuizById = async (id: number) => {
  const sql: string = `
    SELECT q.id as quizID, q.title as quizTitle, q.category_id as quizCategory, q.users_id as quizCreatorID, q.timelimit,
            qs.id as questionID, qs.text as questionText, qs.points as questionPoints,
            a.id as answerID, a.text as answerText, a.is_correct
    FROM quiz as q, questions as qs, answers as a
    WHERE q.id = ? AND qs.quiz_id = ? AND qs.id = a.questions_id;
    `;

  return await pool.query(sql, [id, id]);
};

const getUserQuizzes = async (userId: number) => {
  const sql: string = `
  SELECT q.id, q.title, q.timelimit, c.name as category
  FROM quiz as q, categories as c
  WHERE q.users_id = ? AND c.id = q.category_id
  ORDER BY q.id DESC
  LIMIT 5;
    `;

  return await pool.query(sql, [userId]);
};
const quizSubmissions = async (quizId: number) => {
  const sql: string = `
  SELECT DISTINCT u.username, u.firstname, u.lastname, t.started_at, t.result as score
  FROM takes as t, users as u
  WHERE u.roles_id = 2 AND u.id = t.users_id AND t.quiz_id = ? 
  ORDER BY t.started_at DESC;
    `;

  return await pool.query(sql, [quizId]);
};
const getQuizzes = async () => {
  const sql: string = `
    SELECT * FROM quiz
    `;

  return await pool.query(sql, []);
};

const checkQuizByName = async (name: string) => {
  const sql: string = `
        SELECT q.title
        FROM quiz AS q
        WHERE q.title = ?;
    `;
  const result = await pool.query(sql, [name]);
  return result[0];
};

const createQuiz = async (quiz: Quiz) => {
  let sql: string = `INSERT INTO quiz(title, category_id, users_id, timelimit)
        VALUES ("${quiz.title}", ${quiz.category}, ${quiz.userId}, ${quiz.timelimit});
        SELECT id INTO @QUIZ_ID FROM quiz WHERE quiz.title = "${quiz.title}";
    `;

  const questions = [...quiz.questions];

  for (const question of questions) {
    sql += `
            INSERT INTO questions(quiz_id, text, points)
            VALUES (@QUIZ_ID, "${question.name}", ${question.points});
            SELECT id INTO @QUESTION_ID FROM questions AS q WHERE q.text = "${question.name}" AND q.quiz_id = @QUIZ_ID;
        `;

    const answers = [...question.answers];

    for (const answer of answers) {
      sql += `
                INSERT INTO answers(questions_id, text, is_correct)
                VALUES (@QUESTION_ID, "${answer.text}", ${answer.is_correct});
        `;
    }
  }
  const connection = await pool.getConnection();
  await connection.beginTransaction();
  connection.query(sql);
  connection.commit();
};

export default {
  getQuizById,
  getUserQuizzes,
  quizSubmissions,
  getQuizzes,
  checkQuizByName,
  createQuiz,
};
