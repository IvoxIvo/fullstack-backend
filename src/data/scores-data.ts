import pool from './pool.js';

const getAllScores = async (limit: number, offset: number) => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result 
  FROM takes,users 
  WHERE users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [limit, offset]);
  return result;
};

const getAllScoresCount = async () => {
  const sql: string = `
  SELECT COUNT(DISTINCT takes.users_id) as count
	FROM takes,users 
  WHERE users.id=takes.users_id 
	AND users.roles_id = 2
	AND takes.result <> -1
	AND takes.is_submitted = 1;
  `;

  const result = await pool.query(sql);
  return result;
};

const searchScoresBy = async (
  search: string,
  limit: number,
  offset: number
) => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result 
  FROM takes,users 
  WHERE LOWER(users.username) LIKE LOWER('%${search}%')and users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [limit, offset]);
  return result;
};

const searchScoresCount = async (search: string) => {
  const sql: string = `
  SELECT COUNT(*) as count
  FROM takes,users
  WHERE LOWER(users.username) LIKE LOWER('%${search}%')
  AND users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id;
  `;

  const result = await pool.query(sql);
  return result;
};

const topScores = async () => {
  const sql: string = `
  SELECT users.firstname, users.lastname, users.username, SUM(takes.result) AS result 
  FROM takes,users 
  WHERE users.id=takes.users_id 
  AND users.roles_id = 2
  AND takes.is_submitted=1
  GROUP BY users_id
  ORDER BY result
  DESC
  LIMIT 10;  
  `;

  const result = await pool.query(sql);
  return result;
};

const findQuiz = async (id: number) => {
  const sql: string = `
  SELECT * 
  FROM quiz 
  WHERE id = ?
  `;

  const result = await pool.query(sql, [id]);
  return result[0];
};

const quizScores = async (id: number, limit: number, offset: number) => {
  const sql: string = `
  SELECT * 
  FROM takes 
  WHERE quiz_id = ? 
  AND result <> -1 
  ORDER BY result 
  DESC
  LIMIT ? OFFSET ?;
  `;

  const result = await pool.query(sql, [id, limit, offset]);
  return result;
};

const quizScoresCount = async (id: number) => {
  const sql: string = `
  SELECT COUNT(id) as count
  FROM takes 
  WHERE quiz_id = ? 
  AND result <> -1;
  `;

  const result = await pool.query(sql, [id]);
  return result[0].count;
};

export default {
  getAllScores,
  getAllScoresCount,
  searchScoresBy,
  searchScoresCount,
  topScores,
  findQuiz,
  quizScores,
  quizScoresCount,
};
