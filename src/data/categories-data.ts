import pool from './pool.js';

const getCategoryBy = async (
  searchBy: string,
  column: string,
  limit: number,
  offset: number
) => {
  const sql: string = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE LOWER(c.${column}) LIKE '%${searchBy}%' AND i.id = c.images_id
  LIMIT ? OFFSET ?;
  `;

  return await pool.query(sql, [limit, offset]);
};

const getSearchCategoryCount = async (searchBy: string, column: string) => {
  const sql: string = `
  SELECT COUNT(*) as count FROM categories 
  WHERE ${column} LIKE '%${searchBy}%' 
  `;

  return await pool.query(sql);
};

const getAllCategories = async (limit: number, offset: number) => {
  const sql: string = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE i.id = c.images_id
  LIMIT ? OFFSET ?;
  `;
  return await pool.query(sql, [limit, offset]);
};

const allCategoriesCount = async () => {
  const sql: string = `
  SELECT COUNT(*) as count FROM categories 
  `;

  return await pool.query(sql);
};

const categoryBy = async (column: string, getBy: string | number) => {
  const sql: string = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE c.${column} = ? && i.id = c.images_id;
  `;
  const result = await pool.query(sql, [getBy]);
  return result[0];
};

const allCategories = async () => {
  const sql: string = `
  SELECT c.id, c.users_id, c.name, c.description, c.images_id, i.path
  FROM categories as c, images as i
  WHERE i.id = c.images_id;
  `;

  return await pool.query(sql);
};

const allCategoryImages = async () => {
  const sql: string = `
  SELECT *
  FROM images;
  `;

  return await pool.query(sql);
};

const createCategory = async (
  name: string,
  image: number,
  userId: number,
  description: string | undefined
) => {
  const sql: string = `
  INSERT INTO categories(name, description, images_id, users_id)
  VALUES (?, ?, ?, ?)
  `;

  const result = await pool.query(sql, [name, description, image, userId]);

  return {
    id: result.insertId,
    name: name,
    description: description,
    image: image,
    userId: userId,
  };
};

const quizzesOfCategory = async (id: string, userId: number) => {
  const sql: string = `
  SELECT DISTINCT quiz.id as quiz_id ,quiz.title,categories.name, 
  EXISTS(
  SELECT takes.is_submitted 
  FROM takes 
  WHERE takes.is_submitted=1 
  AND takes.users_id = ?
  AND takes.quiz_id=quiz.id
  ) as taken_quiz
  FROM categories, quiz
  WHERE categories.id = ?
  AND categories.id = quiz.category_id
  ORDER BY taken_quiz;
  `;
  const result = await pool.query(sql, [userId, id]);

  return result;
};

export default {
  getCategoryBy,
  getSearchCategoryCount,
  getAllCategories,
  allCategoriesCount,
  categoryBy,
  allCategories,
  allCategoryImages,
  createCategory,
  quizzesOfCategory,
};
