import pool from './pool.js';

const findUserTake = async (id: number, userId: number) => {
  const sql: string = `
  SELECT *
  FROM takes 
  WHERE users_id = ? AND quiz_id = ?
  `;

  const result = await pool.query(sql, [userId, id]);
  return result[0];
};

const getActiveQuiz = async (userId: number) => {
  const sql: string = `
  SELECT *
  FROM takes
  WHERE users_id = ? AND is_submitted = 0
  `;

  const result = await pool.query(sql, [userId]);
  return result[0];
};

const newTake = async (id: number, userId: number, startingTime: Date) => {
  const sql: string = `
  INSERT INTO takes(quiz_id, users_id, started_at, is_submitted)
  VALUES (?, ?, ?,'0');
  `;

  const result = await pool.query(sql, [id, userId, startingTime]);
  return {
    id: result.insertId,
    quiz_id: id,
    users_id: userId,
    started_at: startingTime,
    is_submitted: '0',
  };
};

const submitAnswers = async (id: number, answers: string[]) => {
  let sql: string = answers.reduce((acc, el) => {
    return (acc += `INSERT INTO take_answers(take_id, answers_id)VALUES (${id}, ${el});
    `);
  }, ``);

  return await pool.query(sql);
};

const submitQuiz = async (id: number, userId: number, totalPoints: number) => {
  const sql: string = `
  UPDATE takes
  SET is_submitted = 1, result = ?
  WHERE quiz_id = ? AND users_id = ? AND is_submitted = 0;
  `;
  const result = await pool.query(sql, [totalPoints, id, userId]);

  return result;
};

const getStudentScore = async (id: number) => {
  const sql: string = `
  SELECT avg(takes.result)as avg_result, count(takes.id) as total_takes, users.username, users.firstname,users.lastname 
  FROM takes, users 
  WHERE users.roles_id=2 
  AND takes.users_id = ?  
  AND users.id = takes.users_id
  AND takes.is_submitted=1;
  `;

  const result = await pool.query(sql, [id]);

  return result;
};

const getScoreOnTake = async (id: number, userId: number) => {
  const sql: string = `
  SELECT takes.result, users.username, users.firstname,users.lastname 
  FROM takes, users 
  WHERE users.roles_id = 2
  AND takes.quiz_id = ? 
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1;
  `;

  const result = await pool.query(sql, [id, userId]);
  return result[0];
};

const getLastUserScores = async (userId: number) => {
  const sql: string = `
  SELECT takes.id as takes_id, takes.quiz_id, takes.users_id, takes.started_at, takes.is_submitted, takes.result,users.username,users.firstname,users.lastname, quiz.title
  FROM takes,users,quiz
  WHERE users.roles_id = 2
  AND takes.quiz_id=quiz.id
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1
  ORDER BY takes.id 
  DESC
  LIMIT 5;
  `;

  const result = await pool.query(sql, [userId]);
  return result;
};

const getCorrectAnswers = async (id: number) => {
  const sql: string = `
  SELECT questions.id as question_id, answers.id as answers_id, questions.points, answers.text
  FROM questions, quiz , answers
  WHERE quiz.id = questions.quiz_id 
  AND quiz.id = ?
  AND answers.is_correct = 1
  AND answers.questions_id = questions.id
  ORDER BY question_id;  
  `;

  const result = await pool.query(sql, [id]);
  return result;
};

const getUserAnswers = async (id: number, userId: number, takeId: number) => {
  const sql: string = `
  SELECT questions.id as question_id, take_answers.answers_id as answer_from_take
  FROM questions, quiz , take_answers, takes ,answers
  WHERE quiz.id = questions.quiz_id
  and answers.questions_id = questions.id
  and answers.id = take_answers.answers_id
  AND takes.id = take_answers.take_id
  AND takes.quiz_id =quiz.id
  AND takes.id = ?
  AND quiz.id = ?
  AND takes.users_id = ?;
  `;

  const result = await pool.query(sql, [takeId, id, userId]);

  return result;
};

const getUserScores = async (userId: number, limit: number, offset: number) => {
  const sql: string = `
  SELECT takes.id as takes_id, takes.quiz_id, takes.users_id, takes.started_at, takes.is_submitted, takes.result,users.username,users.firstname,users.lastname, quiz.title
  FROM takes,users,quiz
  WHERE users.roles_id = 2
  AND takes.quiz_id=quiz.id
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1
  ORDER BY takes.id 
  DESC
  LIMIT ?
  OFFSET ?
  `;

  const result = await pool.query(sql, [userId, limit, offset]);
  return result;
};

const getUserScoresCount = async (userId: number) => {
  const sql: string = `
  SELECT COUNT(DISTINCT takes.id) as count
  FROM takes,users,quiz
  WHERE users.roles_id = 2
  AND takes.quiz_id=quiz.id
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1;
  `;

  const result = await pool.query(sql, [userId]);
  return result;
};

const searchUserScoresBy = async (
  userId: number,
  search: string,
  limit: number,
  offset: number
) => {
  const sql: string = `
  SELECT takes.id as takes_id, takes.quiz_id, takes.users_id, takes.started_at, takes.is_submitted, takes.result,users.username,users.firstname,users.lastname, quiz.title
  FROM takes,users,quiz
  WHERE users.roles_id = 2
  AND takes.quiz_id=quiz.id
  AND LOWER(quiz.title) LIKE LOWER('%${search}%')
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1
  ORDER BY takes.id 
  DESC
  LIMIT ?
  OFFSET ?
  `;

  const result = await pool.query(sql, [userId, limit, offset]);
  return result;
};

const searchUserScoresCount = async (userId: number, search: string) => {
  const sql: string = `
  SELECT COUNT(DISTINCT takes.id) as count
  FROM takes,users,quiz
  WHERE users.roles_id = 2
  AND takes.quiz_id=quiz.id
  AND LOWER(quiz.title) LIKE LOWER('%${search}%')
  AND takes.users_id = ?
  AND users.id = takes.users_id
  AND takes.is_submitted = 1;
  `;

  const result = await pool.query(sql, [userId]);
  return result;
};

export default {
  findUserTake,
  newTake,
  getActiveQuiz,
  submitAnswers,
  submitQuiz,
  getStudentScore,
  getScoreOnTake,
  getLastUserScores,
  getCorrectAnswers,
  getUserAnswers,
  getUserScores,
  getUserScoresCount,
  searchUserScoresBy,
  searchUserScoresCount,
};
